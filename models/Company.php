<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $com_name
 * @property string $com_product
 * @property string $addrress
 * @property string $phone_number
 */

class Company extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'company';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['com_name', 'com_product', 'addrress', 'phone_number'], 'required'],
			[['com_name', 'com_product', 'addrress', 'phone_number'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'com_name'     => 'Company Name',
			'com_product'  => 'Company Product',
			'addrress'     => 'Addrress',
			'phone_number' => 'Phone Number',
		];
	}
}
