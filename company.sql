-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2018 at 05:16 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(50) NOT NULL,
  `com_name` varchar(255) NOT NULL,
  `com_product` varchar(255) NOT NULL,
  `addrress` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `com_name`, `com_product`, `addrress`, `phone_number`) VALUES
(1, 'Lucky Company', 'Purified Drinking Water', '489 Tampawadi,Chanmya Tharzi Township,Mandalay,Myanmar', '02-59034'),
(2, 'Loi Hein Company', 'Purified Drinking Water', 'Chan Aye Thar Zan TownShip,Mandalay', '09-73097136'),
(3, 'Mother Company', 'Trading & Construction ', 'Kywesekan Qr,Pyigyitagon Township,Mandalay', '02-80865'),
(4, 'Gold Fuji Company', 'Food and Cosmestics', '494,Bet 80*30 Street,Mandalay', '02-73680'),
(5, 'Shwe Thitsar Company', 'Cement & Steel', 'Street 81,Bet 29*30 ,Mandalay', '02-39413'),
(6, 'Pyae Sone Company', 'Scale', '153, St 74,Bet 29*30 ,Mandalay', '02-64415'),
(7, 'MCPF Company', 'Paperbox', 'AyeZaYat Mamdan Quarter,AMaRaPuRa,Mandalay', '02-70034'),
(8, 'Myint Company', 'Furniture', '4, St35,Bet 61*62 ,Mandalay', '02-62263'),
(9, 'Madina Family Company', 'Cement & Steel', 'No 189, St 82,Bet 21*22 St, Mandalay', '02-35105'),
(10, 'Jinlong Myanmar Company', 'Machinary', 'No 493,St 82,Bet 35*36 St,Mandalay', '02-21438'),
(11, 'Sweety Home Company', 'Bedding & Sofa', 'No 236,St 33,Bet 81*82 St,Mandalay', '09-91029989'),
(12, 'Silver Tiger Company', 'Lighting & chandelier', 'No 268, St 26,Bet 83*84,Mandalay', '02-34957'),
(13, 'Shwe Htun Company', 'Truck', 'No (3-1), St 72 ,Mandalay', '02-80527'),
(14, 'Swan Inn Company', 'Steel', 'St 62,Chan Mya Thar Zi,Mandalay', '09-2004375'),
(15, 'VS International Company', 'Electronic', 'St 77,Bet 32*33 St,Mandalay', '02-30791'),
(16, 'CZM Company', 'Tires', 'St 66, YawMinGyin Quarter,Mandalay', '02-5154403'),
(17, 'Billion Gain Company', 'Electronic', 'St 36,Bt 81*82 ,Maharaungmyay Township,Mandalay', '02-71824'),
(18, 'Triple Circle Company', 'Cable', 'No 784,Bt 62*63 St, Mandalay', '02-72965'),
(19, 'Myanmar Central Trading Company', 'steel', 'St 64,Bet 41*42 St,Mandalay', '09-43063369'),
(20, 'Shwete Company', 'Shampoo & conditioner', 'No.552,St 80,Bet 33*34 St,Mandalay', '02-378195'),
(21, 'New Star Light Company', 'Construction', 'Corner of St 29,Chan Aye Thar Zan Township,Mandalay', '02-24562'),
(22, 'Tun Tun Win Company', 'Track & car', 'St 61,Bet 34*35 St,Mandalay', '02-5154855'),
(23, 'San Thit Oo Company', 'Construction materials', 'St 12,Aungmyaytharzan Township,Mandalay', '02-78659'),
(24, 'New Super Company', 'Plastic Printing & Production', 'No 3,Yangon-Mandalay Road,Mandalay', '09-5184594'),
(25, 'Venus Company', 'Plastic packaging printing', 'St 80,Bet 15*16 St,Mandalay', '02-31124'),
(26, 'Sunlight Company', 'Curtain', 'St 80,Corner of 37 St,Mandalay', '09-91014808'),
(27, 'Myanmar White Elephant Company', 'Digital scale', 'No.202,St 31,Bet 83*84 St,Mandalay', '09-73079688'),
(28, 'YUASA Company', 'Car Battery', 'St 82,Bet 27*28 St,Mandalay ', '02-60994'),
(29, 'Honey Company', 'Raw material of Cake,Bakery & Jelly', 'St 83,Corner of 30 St,Mandaly', '02-32211'),
(30, 'Yadanar Moe Company ', 'Books', 'No.54,Aye Chan Thar Zan Township,MAndalay', '02-33578'),
(31, 'Seagull Company', 'Steel materials of kitchen,hotel', 'No.80,Bet 31*32 St,Mandalay', '02-60148'),
(32, 'Giorenzo Company', 'Clothing', 'St 80,Bet 30*31 St ,Mandalay', '09-980254'),
(33, 'Golden Lion Company', 'Wire & Cable', 'No.104,St 62,Bet 30*31 St,Mandalay', '02-65585'),
(34, 'Grand Royal Company', 'Alcohol', 'St 32,Bet 76*77 St,Mandalay', '02-69569'),
(35, 'Mercury Rays Company', 'Floor Coating', 'No.405,Corner of 31 St,Mandalay', '09-794456'),
(36, 'Oo Te Bwar Company', 'Building construction', 'No.64,Bet 33*34 St,Bet 85*86 St,Mandalay', '09-2012532'),
(37, 'Myint Zu Thar Company', 'Jewellary', 'Bet 69*71 St,Manawhaery Road,Mandalay', '02-77825'),
(38, 'Five Hero Company', 'Cable & Wire', 'Yangon-Mandaly Road,Mandalay', '09-2005838'),
(39, 'Seven Star Company', 'Brick', 'St 81,Bet 33*34 St,Mandalay', '02-66435'),
(40, 'Shwe Lat Shan Company', 'Brick ', 'St 62,PyiGyiTaKon Township,Mandalay', '09-256094620'),
(41, 'Sky Blue Company', 'Peter bags', 'Bet 59*60 St,Bet 32*35 St,Mandalay', '02-74683'),
(42, 'Global Power Company', 'Mobile phone & laptop', 'No.123,Bet 35*36 St,Mandalay', '02-234667'),
(43, 'Ocean Glory Company', 'Furniture', 'St 29,Bet 79*80 St,Mandalay', '02-36243'),
(44, 'I love it Company', 'Shoes', 'St 78,Bet 34*35 St,Mandalay', '09-794578'),
(45, 'Shwe Sin Min Company', 'Noodle', 'No.302,Bet 81*82 St,Mandalay', '02-445590'),
(46, 'Lucky Company', 'Egg Noodle', 'St 25,Bet 86*87 St,Mandalay', '09-2042087'),
(47, 'Sein Nan Daw Company', 'Gold', 'st 26,Bet 88*89 St,Mandalay', '02-208994'),
(48, 'Great Wall Company', 'Sugar', 'St 84,Bet 38*39 St,Mandalay', '09-96645'),
(49, 'Myat TawWin Company', 'Medicine', 'Yangon-Mandalay Road,Mandalay', '02-237865'),
(50, 'Cherry Oo Company', 'Clock & Watch', 'St 26,Bet 84*85 St,Mandalay', '02-218890'),
(51, 'Great One Company', 'Paper Box', 'H/222 53 St,Mandalay', '02-5154946'),
(52, 'KTM Company', 'Water', '80 St,Bet 36*37, Mandalay', '02-76158'),
(53, 'Three Dragon Company', 'Coal', '22 St,Bet 82*83, Mandalay', '02-69725'),
(54, 'Technoland Company', 'Computer & Mobile', '84 St, Bet 32*33,Mandalay', '02-77435'),
(55, 'Ayeyarwady Partners Trading co.,ltd', 'Car Accessories', 'Pyigyitagon Quarter, Mandalay', '09-402634566'),
(56, 'Royal Star Company', 'Roofing', '119/ St23,Mandalay', '02-32516'),
(57, 'Gold Company', 'Furniture', 'No(2/118) Bet 52*53, Mandalay', '09-91001438'),
(58, 'Mandalay Stars Co.,Ltd', 'Gas & Welding Accessaries', 'St35, Bet80*81, Mandalay', '0271878'),
(59, 'Aha Company', 'Purified Drinking Water', 'St9, Pyigyitagon Tsp,Mandalay', '02-59995'),
(60, 'Royal White Fruits Production Co.,Ltd', 'Fruit Jam', 'St78, Bet30*31,Mandalay', '02-35086'),
(61, 'Five Star Company', 'Gold Machinery', 'St51,Mandalay', '02-5154331'),
(62, 'Royal Shinning Star Co.,Ltd', 'WoodIndustry & WoodTrading', 'St68,Pyigyitagon Tsp,Mandalay', '02-5152263'),
(63, 'Gold Standard Company', 'Painting', 'St26,Bet90*91, Mandalay', '02-66098'),
(64, 'Good Brother Company', 'Machinery', 'St81,Bet35*36,Mandalay', '02-39948'),
(65, 'Sweety Life Company', 'Furniture & Interior', 'Bet66*67,Theik Pan St,Mandalay', '02-77267'),
(66, 'Wounder House Company', 'Kitchen & Bed Room Furniture', 'St35,Bet77*78,Mandalay', '02-60230'),
(67, 'True Brother Company', 'Gems & Jewellery', 'St16,Bet86*87, Mandalay', '09-2031873'),
(68, 'Yee Shin Company', 'Phone Sales & Services', 'St80,Bet33*34,Mandalay', '02-30880'),
(69, 'Yadanabon Glass Co.Ltd', 'Flat & Bend Tempered Glass', 'St75, Bet33*34,Mandalay', '02-73941'),
(70, 'Myanmar Apple Company', 'Digital Printing', 'St61,Bet55*56,Mandalay', '09-5415013'),
(71, 'Sein Thiha Company', 'Plastic Printing Work', 'St61,Ayeyawady Corner,Mandalay', '02-5154183'),
(72, 'Royal Flex Company', 'Wire & Cable', 'St30,Bet77*78,Mandalay', '09-8615271'),
(73, 'Triple \"A\" International Company', 'Ready Mixed Concrete', 'St81,Bet30*31,Mandalay', '09-6502711'),
(74, 'Triple Nine Company', 'Sunflower Oil', '64/ Bet33*34,Bet85*86,Mandalay', '09-47125877'),
(75, 'SMA Co.,Ltd', 'Stainless Steel Water Tank ', 'Yangon_Mandalay Highway Road', '09-2056718'),
(76, 'Amazing One Company', 'Construction, Mining, Gems & Trading', 'St81,Bet36*37,Mandalay', '09-2003788'),
(77, 'Vivala Company', 'Clothing', 'St42,Bet66*67,Mandalay', '09-2024386'),
(78, 'Zalatwah Company', 'Cookies & Cakess', 'St32,Bet83*84,Mandalay', '02-21295'),
(79, 'Golden Lion Company', 'Plastic', 'St85,Bet36*37,Mandalay', '02-252550'),
(80, 'Hyper Sonic Trading Company', 'Acid Dyes', 'St78,Bet51*52,Mandalay', '02-501265'),
(81, 'New World Commpany', 'Cotton Cloth', 'St83,Bet23*24,Mandalay', '09-788957356'),
(82, 'Phado Company', 'Travel & Tour', 'St75,Bet42*43,Mandalay', '02-5154566'),
(83, 'Shwe Company', 'Food', 'St64,Bet71*72,Mandalay', '09-798956321'),
(84, 'Charles & Keth', 'Bag & Shoe', 'St69,Bet36*37,Mandalay', '02-501326'),
(85, 'Michael Cors Company', 'Clock & Watch', 'St73,Bet63*64,Mandalay', '09-5021654'),
(86, 'Bwin Company', 'Body Shower & Shampoo', 'St82,Bet41*42,Mandalay', '02-512364'),
(87, 'Billion Gain Manufacturing Co.,Ltd', 'Schneider Electric', 'St36,Bet81*82,Mandalay', '02-71824'),
(88, 'Nataional Company', 'Gas', 'St80,Bet28*29,Mandalay', '02-341104'),
(89, 'Shwete Company', 'Shampoo & Conditioner', 'St80,Bet33*34,Mandalay', '02-39103'),
(90, 'New Super Indudtrial Company', 'Plastic Printing', 'St64,Beet34*34,Mandalay', '951-400136'),
(91, 'Green Apple Company', 'Tape', 'St69,Bet50*51,Mandalay', '02-526398'),
(92, 'Golden Light Company', 'Fan', '78Road,Conor of 39 Street,Mandalay', '09-962364123'),
(93, 'Yamasue Company', 'Umbrella', 'St65,Bet65*66,Mandalay', '02-539841'),
(94, 'Thiha Aung Company', 'Motorcycle', 'St78,Bet39*40,Mandalay', '09-960521365'),
(95, 'Super Mobile Company', 'Retail & Wholesale Service Center', 'St78,Bet27*28,Mandalay', '02-68405'),
(96, 'Singapore Company', 'Glass', 'St27,83Conor,Mandalay', '09-91007667'),
(97, 'Hello Kitty Company', 'Doll', 'St61,Bet39*40,Mandalay', '02-2030239'),
(98, 'Htun Tauk Company', 'Bags & Handbag', 'St69.Bet98*99,Mandalay', '09-910032654'),
(99, 'J_Hervy Company', 'Ball Point Pen', '83*36of Conor,Mandalay', '09-2062119'),
(100, 'Industry Company', 'Super Glue Instant', 'St87,Bet61*62,Mandalay', '09-777435568');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
