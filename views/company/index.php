<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Companysearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Mandalay Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?=Html::encode($this->title)?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<p>
<?=Html::a('Add Company', ['create'], ['class' => 'btn btn-success'])?>
</p>

<?=GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'layout'       => "{summary}\n{items}\n<div class='text-center'>{pager}</div>",
		'columns'      => [
			['class'      => 'yii\grid\SerialColumn'],

			// 'id',
			'com_name',
			'com_product',
			'addrress',
			'phone_number',

			['class' => 'yii\grid\ActionColumn'],
		],
	]);?>
</div>
