<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="company-form">

<?php $form = ActiveForm::begin();?>

<?=$form->field($model, 'com_name')->textInput(['maxlength' => true])?>

<?=$form->field($model, 'com_product')->textInput(['maxlength' => true])?>

<?=$form->field($model, 'addrress')->textInput(['maxlength' => true])?>

<?=$form->field($model, 'phone_number')->textInput(['maxlength' => true])?>
<div class="form-group">
<?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>

<?=Html::a('Back', ['index'], ['class' => 'btn btn-warning'])?>
</div>

<?php ActiveForm::end();?>
</div>
